const Recipe = require("../models/Recipe");
const CategoryService = require("./category");
class RecipeService {
  constructor() {}

  async createRecipe(recipe) {
    const Category = new CategoryService();
    try {
      const newRecipe = new Recipe(recipe);
      await Category.getCategoryById(newRecipe.category);
      await newRecipe.save();
      return newRecipe;
    } catch (e) {
      throw new Error(e);
    }
  }

  async getRecipe(id) {
    const recipe = await Recipe.findById(id);
    if (!recipe) {
      throw new Error("Recipe not found");
    }

    return recipe;
  }

  async updateRecipe(recipe) {
    try {
      const updRecipe = await Recipe.updateOne(recipe);
      return updRecipe;
    } catch {
      throw new Error("Update failed");
    }
  }

  async deleteRecipe(id) {
    try {
      console.log(id);
      Recipe.findByIdAndDelete(id).then((deletedRecipe) => {
        if (deletedRecipe) {
          console.log("Recipe deleted successfully:", deletedRecipe);
        } else {
          console.log("Recipe not found!");
        }
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  async listRecipes() {
    try {
      const recipes = await Recipe.find({});
      return recipes;
    } catch (e) {
      throw new Error(e);
    }
  }

  async listRecipesByQuery(queryString) {
    try {
      const recipes = await Recipe.find({
        $or: [
          { title: { $regex: queryString, $options: "i" } },
          { description: { $regex: queryString, $options: "i" } },
        ],
      })
        .populate("category")
        .exec();

      return recipes;
    } catch (error) {
      // Handle any errors that occur during the query
      throw new Error("Error retrieving recipes: " + error.message);
    }
  }

  async listRecipesByCategory(categoryId) {
    try {
      const recipes = await Recipe.find({ category: categoryId });
      return recipes;
    } catch (e) {
      throw new Error(e);
    }
  }
}

module.exports = RecipeService;
