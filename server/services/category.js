const Category = require("../models/Category");

class CategoryService {
  constructor() {}

  async createCategory(category) {
    try {
      const newCategory = new Category(category);
      await newCategory.save();
      return newCategory;
    } catch (e) {
      throw new Error(e);
    }
  }

  async getCategoryById(id) {
    try {
      const category = await Category.findById(id);
      if (!category) {
        throw new Error("This category doesnt exist.");
      }
      return category;
    } catch (e) {
      throw new Error("Invalid category id");
    }
  }

  async getCategoryItems(id) {
    const category = await Category.findById(id);
    if (!category) {
      throw new Error("Category not found");
    }

    return category;
  }

  async deleteCategory(id) {}

  async listCategories() {
    try {
      const categories = await Category.find({});
      return categories;
    } catch (e) {
      throw new Error(e);
    }
  }
}

module.exports = CategoryService;
