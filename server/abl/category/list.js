const CategoryService = require("../../services/category");

const Category = new CategoryService();

async function ListCategories(req, res, next) {
  try {
    const categories = await Category.listCategories();
    if (categories.length === 0) {
      return res.status(200).send("No categories found");
    }
    return res.status(200).send(categories);
  } catch (e) {
    return res.status(400).send(e);
  }
}

module.exports = ListCategories;
