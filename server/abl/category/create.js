const CategoryService = require("../../services/category");

const Category = new CategoryService();

async function CreateCategory(req, res, next) {
  try {
    const category = req.body;

    const newCategory = await Category.createCategory(category);
    return res.json(newCategory);
  } catch (e) {
    return res.status(400).send({
      error: e.message,
    });
  }
}
module.exports = CreateCategory;
