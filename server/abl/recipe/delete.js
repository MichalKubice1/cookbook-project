const RecipeService = require("../../services/recipe");

const Recipe = new RecipeService();

async function DeleteRecipe(req, res) {
  try {
    const recipeId = Object.keys(req.body)[0];
    console.log("We are in abl-recipe-delete");
    Recipe.deleteRecipe(recipeId);
    return res.status(200).send({
      action: `Successfully deleted recipe with id: ${recipeId}`,
    });
  } catch (e) {
    return res.status(400).send({
      error: e.message,
    });
  }
}

module.exports = DeleteRecipe;
