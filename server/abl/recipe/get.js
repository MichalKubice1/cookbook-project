const RecipeService = require("../../services/recipe");

const Recipe = new RecipeService();

async function GetRecipe(req, res, next) {
  try {
    const recipeId = req.params.recipeId;
    const recipe = await Recipe.getRecipe(recipeId);
    return res.status(200).send(recipe);
  } catch (e) {
    return res.status(400).send("Recipe not found");
  }
}

module.exports = GetRecipe;
