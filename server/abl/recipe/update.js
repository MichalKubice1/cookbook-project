const RecipeService = require("../../services/recipe");
const CategoryService = require("../../services/category");
const Recipe = new RecipeService();

async function UpdateRecipe(req, res) {
  try {
    const recipe = req.body;
    const updateRecipe = await Recipe.updateRecipe(recipe);
    return res.json(updateRecipe);
  } catch (e) {
    return res.status(400).send({
      error: e.message,
    });
  }
}

module.exports = UpdateRecipe;
