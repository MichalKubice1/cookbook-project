const CategoryService = require("../../services/category");
const RecipeService = require("../../services/recipe");

const Recipe = new RecipeService();
const Category = new CategoryService();

async function ListRecipes(req, res, next) {
  try {
    const recipes = await Recipe.listRecipes();
    if (!recipes) {
      return res.status(200).send("No recipes found");
    }
    return res.status(200).send(recipes);
  } catch (e) {
    return res.status(400).send(e);
  }
}

async function ListRecipesByQuery(query, res) {
  try {
    const recipes = await Recipe.listRecipesByQuery(query);
    if (!recipes) {
      return res.status(200).send("No recipes found");
    }
    return res.status(200).send(recipes);
  } catch (e) {
    return res.status(400).send(e);
  }
}

async function ListRecipesByCategory(req, res, next) {
  try {
    const category = await Category.getCategoryById(req.params.id);
    const recipes = await Recipe.listRecipesByCategory(category._id);

    if (!recipes) {
      return res.status(200).send([]);
    }

    return res.status(200).send(recipes);
  } catch (e) {
    return res.status(400).send({
      error: e.message,
    });
  }
}
module.exports = { ListRecipes, ListRecipesByCategory, ListRecipesByQuery };
