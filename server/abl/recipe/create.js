const RecipeService = require("../../services/recipe");
const CategoryService = require("../../services/category");
const Recipe = new RecipeService();

async function CreateRecipe(req, res, next) {
  try {
    const recipe = req.body;
    const newRecipe = await Recipe.createRecipe(recipe);
    return res.json(newRecipe);
  } catch (e) {
    return res.status(400).send({
      error: e.message,
    });
  }
}
module.exports = CreateRecipe;
