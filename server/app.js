//načtení modulu express
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

require("dotenv").config();

const recipeRouter = require("./controller/recipe-controller");
const categoryRouter = require("./controller/category-controller");

const mongoDB = process.env.MONGODB;
mongoose
  .connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.log("MongoDB connection error:", err));

//inicializace nového Express.js serveru
const app = express();
//definování portu, na kterém má aplikace běžet na localhostu
const port = process.env.PORT || 8000;

// Parsování body
app.use(express.json()); // podpora pro application/json
app.use(express.urlencoded({ extended: true })); // podpora pro application/x-www-form-urlencoded

app.use(cors());

app.use("/recipe", recipeRouter);
app.use("/category", categoryRouter);

app.get("/*", (req, res) => {
  res.status(500).send("Unknown path!");
});

//nastavení portu, na kterém má běžet HTTP server
app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
