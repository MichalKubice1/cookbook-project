const express = require("express");
const router = express.Router();

const CreateCategory = require("../abl/category/create");
const DeleteCategory = require("../abl/category/delete");
const ListCategories = require("../abl/category/list");

router.post("/create", async (req, res) => {
  await CreateCategory(req, res);
});

router.post("/delete", async (req, res) => {
  await DeleteCategory(req, res);
});

router.get("/list", async (req, res) => {
  await ListCategories(req, res);
});

module.exports = router;
