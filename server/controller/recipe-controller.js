const express = require("express");
const router = express.Router();

const CreateRecipe = require("../abl/recipe/create");
const GetRecipe = require("../abl/recipe/get");
const UpdateRecipe = require("../abl/recipe/update");
const DeleteRecipe = require("../abl/recipe/delete");
const {
  ListRecipes,
  ListRecipesByCategory,
  ListRecipesByQuery,
} = require("../abl/recipe/list");

router.post("/create", async (req, res) => {
  await CreateRecipe(req, res);
});

router.get("/:recipeId", async (req, res) => {
  await GetRecipe(req, res);
});

router.post("/update", async (req, res) => {
  await UpdateRecipe(req, res);
});

router.post("/delete", async (req, res) => {
  await DeleteRecipe(req, res);
});

router.get("/list", async (req, res) => {
  await ListRecipes(req, res);
});

router.get("/query/search", async (req, res) => {
  const q = req.query.search;
  await ListRecipesByQuery(q, res);
});

router.get("/list/:id", async (req, res) => {
  await ListRecipesByCategory(req, res);
});

module.exports = router;
