import React, { useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Showcase from "./Showcase";

import CategoryList from "./CategoryList";

function Dashboard() {
  return (
    <>
      <Row className="mb-4">
        <Col>
          {/* <h4 className="text-center mb-4">Our tips</h4> */}
          {/* <Showcase /> */}
        </Col>
      </Row>

      <Row className="recipeList">
        <CategoryList />
        {/* <RecipeList /> */}
      </Row>
    </>
  );
}

export default Dashboard;
