import React from "react";
import axios from "axios";
import { API_BASE_URL } from "../../config";
import RecipeItem from "./RecipeItem";
import { useEffect, useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const RecipeList = ({ categoryId, query }) => {
  const [recipes, setRecipes] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const url = categoryId
      ? `list/${categoryId}`
      : `query/search?search=${query}`;
    axios.get(`${API_BASE_URL}/recipe/${url}`).then((res) => {
      setRecipes(res.data);
      setLoading(false);
    });
  }, []);

  if (loading) {
    return <div>Loading..</div>;
  }
  if (recipes.length === 0) {
    return (
      <Row>
        <Col>No recipes in this category yet.</Col>
      </Row>
    );
  }
  if (!loading && recipes.length > 0) {
    return (
      <Row className="d-flex align-items-center justify-content-center ">
        {recipes.map((recipe, index) => {
          return <RecipeItem recipe={recipe} key={index} />;
        })}
      </Row>
    );
  }
};

export default RecipeList;
