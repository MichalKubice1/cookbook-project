import React, { useEffect, useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Figure from "react-bootstrap/Figure";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";

import axios from "axios";
import { API_BASE_URL } from "../../config";
const RecipeDetail = ({ recipeId }) => {
  const [recipe, setRecipe] = useState();
  useEffect(() => {
    axios.get(`${API_BASE_URL}/recipe/${recipeId}`).then((res) => {
      setRecipe(res.data);
    });
  }, []);

  if (recipe) {
    return (
      <>
        <Row className="d-flex justify-content-center">
          <Col xs={8} sm={6} md={6} lg={6}>
            <Figure>
              <Figure.Image
                width={371}
                height={380}
                alt="171x180"
                src={recipe.imageUrl}
              />
              <Figure.Caption>{recipe.description}</Figure.Caption>
            </Figure>
          </Col>
        </Row>
        <Row>
          <Col>
            Ingredients:
            <ul className="list-container">
              {recipe.ingredients.map((i, index) => (
                <li className="centered-li" key={index}>
                  {i}
                </li>
              ))}
            </ul>
          </Col>
        </Row>
        <Row>
          <Col>
            Instructions:
            <ol className="list-container">
              {recipe.instructions.map((i, index) => (
                <li className="centered-li" key={index}>
                  {i}
                </li>
              ))}
            </ol>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button
              onClick={() => {
                //`/recipe/${recipeId}`
                axios
                  .post(`${API_BASE_URL}/recipe/update`, recipeId)
                  .then((res) => console.log(res));
              }}
            >
              Update recipe
            </Button>
          </Col>
          <Col>
            <Button
              onClick={() => {
                axios
                  .post(`${API_BASE_URL}/recipe/delete`, recipeId)
                  .then((res) => console.log(res));
              }}
            >
              DELETE recipe
            </Button>
          </Col>
        </Row>
      </>
    );
  } else {
    return <div>Loading...</div>;
  }
};

export default RecipeDetail;
