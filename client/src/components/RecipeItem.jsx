import React from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col, Image, Card } from "react-bootstrap";
const RecipeItem = (props) => {
  return (
    <Col
      className="mb-3 text-center d-flex justify-content-center "
      onClick={() => console.log(props.recipe)}
    >
      <Link to={`/recipes/${props.recipe._id}`} className="recipeItem">
        <Card
          style={{
            width: "18rem",
            height: "20rem",
            overflow: "hidden",
            margin: "10px",
          }}
        >
          <Card.Img
            variant="top"
            src={props.recipe.imageUrl}
            className="recipeImage"
          />
          <Card.Body>
            <Card.Title>{props.recipe.title}</Card.Title>
            <Card.Text>{props.recipe.description}</Card.Text>
          </Card.Body>
        </Card>
      </Link>
    </Col>
  );
};

export default RecipeItem;
