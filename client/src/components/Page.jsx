import React, { useState } from "react";
import Header from "../components/Header";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import RecipeList from "./RecipeList";
import Form from "react-bootstrap/Form";
import { Outlet, Link } from "react-router-dom";
import InputGroup from "react-bootstrap/InputGroup";
import axios from "axios";
import { API_BASE_URL } from "../../config";
const Page = (props) => {
  const [query, setQuery] = useState("");

  return (
    <div className="layout">
      <Header />
      <Row className="recipeList ">
        <Col className="text-center my-3">
          <h1>Delicious Recipes: A Cookbook for Every Occasion</h1>
        </Col>
      </Row>
      <Row className="recipeList mb-4">
        <Col className="d-flex justify-content-center align-center">
          <InputGroup className="mb-3">
            <Form.Control
              placeholder="Search for recipe"
              className="formControl"
              value={query}
              onChange={(e) => setQuery(e.target.value)}
            />
            <Link to={`/recipes/query/${query}`}>
              <Button variant="outline-secondary">Search</Button>
            </Link>
          </InputGroup>
        </Col>
      </Row>
      <Row>
        <Col className="text-center my-3">{props.children}</Col>
      </Row>
    </div>
  );
};

export default Page;
