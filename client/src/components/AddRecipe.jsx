import { useState, useEffect } from "react";
import axios from "axios";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import { storage } from "../../config";
import { API_BASE_URL } from "../../config";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import InputGroup from "react-bootstrap/InputGroup";
import CloseButton from "react-bootstrap/CloseButton";
import ListGroup from "react-bootstrap/ListGroup";

function AddRecipe({ isVisible, setVisible }) {
  const [img, setImg] = useState(null);
  const [categoriesOptions, setCategoriesOptions] = useState([]);
  const [category, setCategory] = useState();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [ingredients, setIngredients] = useState([]);
  const [ingredientsInput, setIngredientsInput] = useState("");
  const [instructionsInput, setInstructionsInput] = useState("");
  const [instructions, setInstructions] = useState([]);

  useEffect(() => {
    axios.get(`${API_BASE_URL}/category/list`).then((res) => {
      setCategoriesOptions(res.data);
    });
  }, []);

  const isFormInvalid =
    title.length < 2 || !img || instructionsInput.length < 0 || !category;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isFormInvalid) {
      return;
    }

    const storageRef = ref(
      storage,
      `files/${img.name}${Math.random().toFixed(2)}`
    );

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, img).then((snapshot) => {
      getDownloadURL(ref(storage, snapshot.metadata.fullPath))
        .then((url) => {
          const recipe = {
            title: title,
            description: description,
            category: category,
            ingredients: ingredients,
            instructions: instructions,
            imageUrl: url,
          };

          axios.post(`${API_BASE_URL}/recipe/create`, recipe).then((res) => {
            setVisible(false);
          });
        })
        .catch((error) => {
          console.log(error);
        });
    });
  };

  const handleClose = () => {
    setVisible(false);
    setTitle("");
    setDescription("");
    setIngredients("");
    setInstructions("");
    setImg(null);
  };

  const handleRemoveIngredient = (index) => {
    const updatedIngredients = [...ingredients];
    updatedIngredients.splice(index, 1);
    setIngredients(updatedIngredients);
  };

  const handleAddIngredient = (value) => {
    if (value.trim() !== "") {
      setIngredients([...ingredients, value]);
      setIngredientsInput("");
    }
  };

  const handleRemoveInstruction = (index) => {
    const updatedInstructions = [...instructions];
    updatedInstructions.splice(index, 1);
    setInstructions(updatedInstructions);
  };

  const handleAddInstruction = (value) => {
    if (value.trim() !== "") {
      setInstructions([...instructions, value]);
      setInstructionsInput("");
    }
  };

  function onImageChange(e) {
    setImg(e.target.files[0]);
  }

  if (categoriesOptions.length > 0) {
    return (
      <>
        <Modal show={isVisible} onHide={handleClose} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Add new recipe</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Row>
                <Col xs={12} sm={6} md={6} lg={4}>
                  <Form.Group className="mb-3" controlId="name">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      value={title}
                      onChange={(e) => setTitle(e.target.value)}
                      type="text"
                      autoFocus
                    />

                    {title.length < 2 ? (
                      <p className="text-error">Atleast 2 characters</p>
                    ) : null}
                  </Form.Group>
                </Col>
                <Col xs={12} sm={6} md={6} lg={4}>
                  <Form.Group className="mb-3" controlId="category">
                    <Form.Label>Category</Form.Label>
                    <Form.Select
                      aria-label="Category"
                      isInvalid={!category}
                      onChange={(event) => setCategory(event.target.value)}
                    >
                      <option></option>
                      {categoriesOptions.map((category, index) => {
                        return (
                          <option key={index} value={category._id}>
                            {category.name}
                          </option>
                        );
                      })}
                    </Form.Select>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group controlId="formFileSm" className="mb-3">
                    <Form.Label>Image</Form.Label>
                    <Form.Control
                      type="file"
                      size="sm"
                      accept="image"
                      onChange={onImageChange}
                    />
                    {!img ? <p className="text-error">Required</p> : null}
                  </Form.Group>
                </Col>
              </Row>

              <Form.Group className="mb-3" controlId="desc">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={2}
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>
              <Row>
                <Col xs={8} sm={6} md={6} lg={6}>
                  <Form.Group controlId="Ingredients">
                    <Form.Label>Ingredients</Form.Label>
                  </Form.Group>
                  <InputGroup className="mb-3">
                    <Form.Control
                      type="text"
                      value={ingredientsInput}
                      onChange={(e) => setIngredientsInput(e.target.value)}
                    />
                    <Button
                      variant="outline-secondary"
                      id="button-addon1"
                      onClick={() => handleAddIngredient(ingredientsInput)}
                    >
                      +
                    </Button>
                  </InputGroup>
                </Col>
                <Col xs={8} sm={6} md={6} lg={4}>
                  {ingredients.length > 0 ? (
                    <ListGroup>
                      {ingredients.map((ing, i) => (
                        <div className="d-flex align-center" key={i}>
                          <ListGroup.Item>{ing}</ListGroup.Item>
                          <CloseButton
                            onClick={() => handleRemoveIngredient(i)}
                          />
                        </div>
                      ))}
                    </ListGroup>
                  ) : (
                    ""
                  )}
                </Col>
              </Row>

              <Row>
                <Col xs={8} sm={6} md={6} lg={6}>
                  <Form.Group controlId="instructions">
                    <Form.Label>Instructions</Form.Label>
                  </Form.Group>
                  <InputGroup className="mb-3">
                    <Form.Control
                      type="text"
                      value={instructionsInput}
                      onChange={(e) => setInstructionsInput(e.target.value)}
                    />
                    <Button
                      variant="outline-secondary"
                      id="button-addon1"
                      onClick={() => handleAddInstruction(instructionsInput)}
                    >
                      +
                    </Button>
                  </InputGroup>
                </Col>
                <Col xs={8} sm={6} md={6} lg={4}>
                  {instructions.length > 0 ? (
                    <ListGroup>
                      {instructions.map((ins, i) => (
                        <div className="d-flex align-center" key={i}>
                          <ListGroup.Item>{ins}</ListGroup.Item>
                          <CloseButton
                            onClick={() => handleRemoveInstruction(i)}
                          />
                        </div>
                      ))}
                    </ListGroup>
                  ) : (
                    ""
                  )}
                </Col>
              </Row>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button
              variant="primary"
              onClick={handleSubmit}
              disabled={isFormInvalid}
            >
              Save recipe
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default AddRecipe;
