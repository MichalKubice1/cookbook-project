import React, { useEffect, useState } from "react";
import { Row, Card, Col } from "react-bootstrap";
import axios from "axios";
import { API_BASE_URL } from "../../config";
import { Link } from "react-router-dom";
const CategoryList = () => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    axios.get(`${API_BASE_URL}/category/list`).then((res) => {
      setCategories(res.data);
      setLoading(false);
    });
  }, []);

  return (
    <Row className="d-flex align-items-center justify-content-around mt-3">
      {categories.map((category, i) => {
        return (
          <Col
            key={i}
            xs={12}
            sm={6}
            md={6}
            lg={4}
            className="mb-3 text-center d-flex align-items-center justify-content-center"
          >
            <Link to={`category/${category._id}`} className="recipeItem">
              <Card style={{ width: "18rem" }}>
                <Card.Img
                  variant="top"
                  src={category.imageUrl}
                  className="recipeImage"
                />
                <Card.Body>
                  <Card.Title>{category.name}</Card.Title>
                  <Card.Text>{category.description}</Card.Text>
                </Card.Body>
              </Card>
            </Link>
          </Col>
        );
      })}
    </Row>
  );
};

export default CategoryList;
