import React, { useState } from "react";
import { Row, Col } from "react-bootstrap";
import Logo from "../assets/img/logo.svg";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import AddRecipe from "./AddRecipe";
import { NavLink } from "react-router-dom";

const Header = () => {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <Row className="d-flex align-items-center header pb-2">
      <Col>
        <NavLink to="/">
          <img src={Logo} className="logo" />
        </NavLink>
      </Col>
      <Col></Col>
      <Col className="d-flex justify-content-end">
        <Button variant="primary" onClick={() => setModalVisible(true)}>
          Add recipe
        </Button>
      </Col>
      <AddRecipe isVisible={modalVisible} setVisible={setModalVisible} />
    </Row>
  );
};

export default Header;
