import React from "react";
import Dashboard from "../components/Dashboard";
import Page from "../components/Page";

const MainPage = () => {
  return (
    <Page>
      <Dashboard />
    </Page>
  );
};

export default MainPage;
