import React, { useState } from "react";
import Page from "../components/Page";
import axios from "axios";
import { Routes, Route, useParams } from "react-router-dom";
import { API_BASE_URL } from "../../config";
import RecipeDetail from "../components/RecipeDetail";
const RecipeDetailPage = () => {
  const [error, setError] = useState(false);
  const [recipe, setRecipe] = useState([]);

  let { recipeId } = useParams();
  if (!error) {
    return (
      <Page>
        <RecipeDetail recipeId={recipeId} />
      </Page>
    );
  } else {
    return <Page>Something went wrong.</Page>;
  }
};

export default RecipeDetailPage;
