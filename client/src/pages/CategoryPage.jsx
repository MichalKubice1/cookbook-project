import React, { useState } from "react";
import Page from "../components/Page";
import { useParams } from "react-router-dom";
import RecipeList from "../components/RecipeList";
const CategoryPage = () => {
  const [error, setError] = useState(false);

  let { categoryId } = useParams();
  if (!error) {
    return (
      <Page>
        <RecipeList categoryId={categoryId} />
      </Page>
    );
  } else {
    return <Page>Something went wrong.</Page>;
  }
};

export default CategoryPage;
