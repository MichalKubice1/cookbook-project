import React, { useState } from "react";
import Page from "../components/Page";
import { useParams } from "react-router-dom";
import RecipeList from "../components/RecipeList";

const RecipesPage = () => {
  let params = useParams();

  return (
    <Page>
      <RecipeList query={params.query} />
    </Page>
  );
};

export default RecipesPage;
