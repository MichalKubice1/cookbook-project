import MainPage from "./pages/MainPage";
import CategoryPage from "./pages/CategoryPage";
import RecipeDetailPage from "./pages/RecipeDetailPage";
import ErrorPage from "./pages/ErrorPage";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import RecipesPage from "./pages/RecipesPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <MainPage />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/recipes",
    element: <CategoryPage />,
  },
  {
    path: "/recipes/:recipeId",
    element: <RecipeDetailPage />,
  },
  {
    path: "/category/:categoryId",
    element: <CategoryPage />,
  },
  {
    path: "/recipes/query/:query",
    element: <RecipesPage />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
