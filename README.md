# Cookbook project

## How to run project locally

1.  Open root folder and and run `npm install`.`
2.  Open folder "server" in terminal and run `npm install`.`
3.  Copy `.env.example` file and name it `.env`. Then fill the values (Ask Ivan!!!)
4.  Get your public IP address from https://whatismyipaddress.com/ and send it to Ivan
5.  Open folder "client" in another terminal folder and run `npm install`.
6.  Copy `config.example.js` file and name it `config.js`. Then fill the values
7.  Go back to root folder and run `npm start`.
8.  App should be running on http://localhost:5174
